##Assessment 3 Readme

```console.log("feel the burn");```

###Eighth Commit

####Frontend
  * Minor rework of register page. Form not passing data from input fields

###Seventh Commit

####Backend
  * Added sessions and passport components for persistence
  * Login and register still does not work

###Sixth Commit

####Front end
  * Changed basic structure of navbar
  * Added Login and Register Page (To be reviewed)
  * Logout Page to be added when time permits

###Fifth Commit

####Front and Back end
  * Tested node controller routing
  * Tested angular controller routing
  * Displaying user list on admin page (Unsecured)

###Forth Commit

####Back end
  * Seeded new data into local mysql database (Unsecured)
  * Noted additional FK fields created automatically on mySQL

###Third Commit

####Back end
  * Tables created successfully on mySQL
  * Found some unnecessary cardinalities on ER Diagram

###Second Commit

####Back end
  * SQL modelling completed
  * Server config files created
  * Sequelize relationships defined
  * Tables not showing on local MySQL workbench

###First Commit

####Cloud deploy
  * Testing bitbucket link to AWS (Failed) 
  * Cloud deploy and sync to be abandoned
####Front end
  * Front end scafolding decided
  * Navigation decided
  * Yet to decide on comment and commentnew