// Loads sequelize ORM
var Sequelize = require("sequelize");
var config = require("./config");

// DBs, MODELS, and ASSOCIATIONS ---------------------------------------------------------------------------------------
// Creates a MySQL connection
// console.log(config.mysql);
// var connection = new Sequelize( config.mysql,
//     {
//         logging: console.log,
//         dialect: 'mysql',   
//         pool: {
//             max: 5,
//             min: 0,
//             idle: 10000
//         }
//     }
// );

// const DB_NAME = "dankmemes";
// const DB_USER = "root";
// const DB_PASSWORD = "rootbeer";
// const DB_HOST = "localhost"

const connection = new Sequelize(
  // DB_NAME, DB_USER, DB_PASSWORD,
  config.mysql,
  {
    // host: DB_HOST,
    dialect: 'mysql',
    logging: console.log,
    pool: {
      max: 5,
      min: 1,
      idle: 10000
    },
    define: {
      timestamps: true
    }
  }
);



var User = require('./models/users')(connection, Sequelize);
var Post = require('./models/posts')(connection, Sequelize);
var Comment = require('./models/comments')(connection, Sequelize);

connection.sync({force: config.seed})
  .then(function (err) { 
    console.log('Item table created successfully');
    require("./seed")();
});

User.hasMany(Post, {foreignKey: 'user_id'});
Post.belongsTo(User);
Post.hasMany(Comment, {foreignKey: 'post_id', foreignKey: 'user_id'});
Comment.belongsTo(Post);
Comment.belongsTo(User);


module.exports = {
    User: User,
    Post: Post,
    Comment: Comment,
    connection: connection
};