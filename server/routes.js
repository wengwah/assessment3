"use strict";

// Loads path to access helper functions for working with files and directory paths
var path = require("path");
var express = require("express");

// Server side controllers
var UserController = require("./api/user/user.controller.js");
var PostController = require("./api/post/post.controller");
var CommentController = require("./api/comment/comment.controller");
// var AWSController = require("./api/aws/aws.controller");

const API_POSTS_URI = "/api/posts";
const API_USERS_URI = "/api/users";
const API_COMMENTS_URI = "/api/comments";

module.exports = function(app, database, passport) {

  // Static path
  const CLIENT_FOLDER = path.join(__dirname + '/../client');
  app.use(express.static(CLIENT_FOLDER));

  // Users API
  // -- CRUD operations --
  app.post(API_USERS_URI, UserController.create);
  app.get(API_USERS_URI + '/:id', UserController.get);
  app.put(API_USERS_URI + '/:id', UserController.update);
  app.delete(API_USERS_URI + '/:id', UserController.remove);

  // -- Admin Dashboard --
  app.get(API_USERS_URI, UserController.list);

  // Post API

  // Comment API

}