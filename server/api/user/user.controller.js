var User = require("../../database").User;
var config = require("../../config");

// Authentication stuffs
// var AuthProvider = require("../../database").AuthProvider;
// var bcrypt = require('bcryptjs');

// Mailgun stuffs
// var api_key = config.mailgun_key;
// var domain = config.mailgun_domain;
// var mailgun = require('mailgun-js')({apiKey: api_key, domain: domain});
// var mailcomposer = require('mailcomposer');


// CRUD Operations
exports.get = function (req, res) {
  User.findById(req.params.id)
    .then(function (user) {
      if (!user) {
        handler404(res);
      }
      res.json(user);
    })
    .catch(function (err) {
      handleErr(res, err);
    });
};

exports.create = function (req, res) {
  User.create(req.body)
    .then(function (user) {
      res.json(user);
    })
    .catch(function (err) {
      handleErr(res, err);
    });
};

exports.update = function (req, res) {  
  User.findById(req.params.id)
    .then(function (user) {
      if (!user) {
        handler404(res);
      }
      res.json(user);
    })
    .catch(function (err) {
      handleErr(res, err);
    });
};

exports.remove = function (req, res) {  
  User.findById(req.params.id)
    .then(function (user) {
      if (!user) {
        handler404(res);
      }
      res.json(user);
    })
    .catch(function (err) {
      handleErr(res, err);
    });
};

// Admin Panel
exports.list = function (req, res) {
  User.findAll()
    .then(function (users) {
      console.log("User List called");
      res.json(users);
    })
    .catch(function (err) {
      handleErr(res, err);
    });
};


// Error Handling 
function handleErr(res) {
  handleErr(res, null);
}


function handleErr(res, err) {
  console.log(err);
  res.status(500).json({
    error: true
  });
}

function handler404(res) {
  res.status(404).json({message: "User not found!"});
}

function returnResults(results, res) {
  res.send(results);
}
