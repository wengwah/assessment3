// var bcrypt   = require('bcryptjs');
var config = require("./config");
var database = require("./database");
var User = database.User;
var Post = database.Post;
var Comment = database.Comment;


module.exports = function () {
  if (config.seed) {
    // var hashpassword = bcrypt.hashSync("password@123", bcrypt.genSaltSync(8), null);
    User.create({        
        user_name: "thor",
        email: "hammertime@heroes.com",
        password: "Password@123",
        user_image: "AvatarM.png",
        join_date: "1999-01-01",
        gender: "M"
      })
      .then(function (users) {
        console.log("user created");
      })
      .catch(function () {
        console.log("Error", arguments);
      });

    User.create({        
        user_name: "wonderwoman",
        email: "diana@heroes.com",
        password: "Password@123",
        user_image: "AvatarF.png",
        join_date: "1999-01-01",
        gender: "F"
      })
      .then(function (users) {
        console.log("user created");
      })
      .catch(function () {
        console.log("Error", arguments);
      });

    User.create({        
        user_name: "superman",
        email: "kent@heroes.com",
        password: "Password@123",
        user_image: "AvatarM.png",
        join_date: "1999-01-01",
        gender: "M"
      })
      .then(function (users) {
        console.log("user created");
      })
      .catch(function () {
        console.log("Error", arguments);
      });

    Post.create({
        caption: "Someone give this busker at Clementi hawker centre a record deal!",
        post_image: "busker.jpg",
        post_details: "I thought that his singing was very impressive. Any idea who he is?",
        create_date: "1999-01-02",
        user_id: 1
      })
      .then(function (posts) {
        console.log("post created");
      })
      .catch(function () {
        console.log("Error", arguments);
      });

    Comment.create({
        comment_image: "wow.gif",
        comment_details: "AMAZING! M I RITE?",
        comment_date: "1999-01-02",
        post_id: 1,
        user_id: 1
      })
      .then(function (comments) {
        console.log("comment created");
      })
      .catch(function () {
        console.log("Error", arguments);
      });
    } // close if

}; // close export
