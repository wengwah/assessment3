var express = require("express");
var bodyParser = require("body-parser");

var cookieParser = require("cookie-parser");
var session = require("express-session");
var passport = require("passport");

var config = require("./config");
const NODE_PORT = config.port;

var app = express();

app.use(cookieParser());

require("./routes")(app);

app.use(bodyParser.urlencoded({ limit: '5mb', extended: true }));
app.use(bodyParser.json({limit: '5mb'}));

app.use(session({
  secret: "ad7e0b08c4b3b325553c90937111d22a188bcbf3d4375d1d7e176df0e7b814af",
  resave: false,
  saveUninitialized: true
}));

app.use(passport.initialize());
app.use(passport.session());

var database = require("./database");
require("./auth")(database, passport);
require("./routes")(app, database, passport);

app.listen(NODE_PORT, function () {
    console.log("Server running at " + config.domain_name);
});

module.exports = app;