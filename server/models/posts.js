module.exports = function(conn, Sequelize) {
  var Post = conn.define('posts', {
    post_id: {
      type: Sequelize.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    caption: {
      type: Sequelize.STRING,
      allowNull: false
    },
    post_image: {
      type: Sequelize.STRING, // point to a location in cloud for uploads, or an api call
      allowNull: false // placeholder, will set to true later
    },
    post_details: {
      type: Sequelize.STRING,
      allowNull: true
    },
    create_date: {
      type: Sequelize.DATE,
      allowNull: false
    },
    user_id: { // created by
      type: Sequelize.INTEGER(11),
      allowNull: false
    }    
  }, {
    tableName: 'posts',
    timestamps: false
  });
  return Post;
};