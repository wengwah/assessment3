module.exports = function(conn, Sequelize) {
  var User = conn.define('users', {
    user_id: {
      type: Sequelize.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    user_name: {
      type: Sequelize.STRING,
      allowNull: false
    },
    email: {
      type: Sequelize.STRING,
      allowNull: false
    },
    password: {
      type: Sequelize.STRING,
      allowNull: false
    },
    user_image: {
      type: Sequelize.STRING, // point to a location in cloud for uploads, or an api call
      allowNull: false
    },
    join_date: {
      type: Sequelize.DATE,
      allowNull: false
    },
    gender: {
      type: Sequelize.ENUM('M','F'),
      allowNull: true
    }
  }, {
    tableName: 'users',
    timestamps: false
  });
  return User;
};