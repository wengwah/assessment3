module.exports = function(conn, Sequelize) {
  var Comment = conn.define('comments', {
    comm_id: {
      type: Sequelize.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    comment_image: {
      type: Sequelize.STRING, // point to a location in cloud for uploads, or an api call
      allowNull: false // placeholder, will set to true later
    },
    comment_details: {
      type: Sequelize.STRING,
      allowNull: false // placeholder, will set to true later
    },
    comment_date: {
      type: Sequelize.DATE,
      allowNull: false
    },
    post_id: {
      type: Sequelize.INTEGER(11),
      allowNull: false
    },
    user_id: {
      type: Sequelize.INTEGER(11),
      allowNull: false
    }
    
  }, {
    tableName: 'comments',
    timestamps: false
  });
  return Comment;
};