(function () {
  "use strict";
  angular
    .module("MyApp")
    .config(MyAppConfig);

  MyAppConfig.$inject = ["$stateProvider", "$urlRouterProvider"];
  
  function MyAppConfig($stateProvider, $urlRouterProvider) {

    $stateProvider
      .state("login", {
        url: "/login",
        templateUrl: "/app/security/login.html",
        controller: "LoginCtrl as ctrl"
      })
      .state("register", {
        url: "/register",
        templateUrl: "/app/security/register.html",
        controller: "RegisterCtrl as ctrl"
      })
      .state("admin", {
        url: "/admin", 
        templateUrl: "/app/admin/admin.html", 
        controller: "AdminCtrl as ctrl" 
      })
      .state("frontpage", {
        url: "/frontpage",
        templateUrl: "/app/frontpage/frontpage.html",
        controller: "FrontPageCtrl as ctrl"
      })
      .state("post", {
        url: "/post", // to change to param route post/:id
        templateUrl: "/app/post/post.html", 
        controller: "PostCtrl as ctrl" 
      })
      .state("postnew", {
        url: "/postnew", 
        templateUrl: "/app/postnew/postnew.html", 
        controller: "PostNewCtrl as ctrl" 
      })
      
    $urlRouterProvider.otherwise("/login");

  } 
    
})();