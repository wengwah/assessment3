(function () {
  angular
      .module("MyApp")
      .controller("LoginCtrl", LoginCtrl);

  LoginCtrl.$inject = ["$state", "AuthService"];

  function LoginCtrl($state, AuthService) {
      var vm = this;
      
      vm.login = function(){
        AuthService.login(vm.user)
          .then(function(){
            console.log("login ...");
            AuthService.isUserLoggedIn(function (result){
              if(result){
                $state.go('register');
              }
              else {
                $state.go('login');
              }
            });
        })
        .catch(function(error){
          console.log("erorr !" + error);
        });
          
      }
  }
})();