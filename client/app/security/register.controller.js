(function () {
  "use strict";
  angular
    .module("MyApp")
    .controller("RegisterCtrl", RegisterCtrl);

  RegisterCtrl.$inject = ["$sanitize", "$state", "MyAppService"];

  function RegisterCtrl($sanitize, $state, MyAppService){
    var vm = this;
    var today = new Date();
    
    // Exposed data models ---------------------------------------------------------------------------------------
    vm.newUser = {
      user_name: "",
      email: "",
      password: "",
      user_image: "",
      join_date: today,
      gender: ""
    };

    // Creates a status object. We will use this to display appropriate success or error messages.
    vm.status = {
      message: "",
      code: ""
    };

    // Exposed functions ------------------------------------------------------------------------------------------
    // Exposed functions can be called from the view.
    vm.register = register;

    // Initializations --------------------------------------------------------------------------------------------
    // Functions that are run when view/html is loaded

    function register() {      
      // Calls alert box and displays registration information
      alert("The registration information you sent are \n" + JSON.stringify(vm.employee));

      MyAppService.createUser(vm.newUser)
        .then(function (results) {
          console.log("result " + JSON.stringify(result));
        })
        .catch(function (err) {
          console.log("error " + JSON.stringify(err));
          vm.status.message = err.data.name;
        })

      $state.go("login");
    }


      
  }
})();