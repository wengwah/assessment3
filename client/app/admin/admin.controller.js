(function () {
  "use strict";
  angular
    .module("MyApp")
    .controller("AdminCtrl", AdminCtrl);

  AdminCtrl.$inject = ["$filter", "$stateParams", "MyAppService"];

  function AdminCtrl($filter, $stateParams, MyAppService) {
    var vm = this; // vm

    vm.searchString = '';
    vm.result = null;
    vm.showDepartment = false;
    vm.users = "";

    // Exposed functions ------------------------------------------------------------------------------------------
    // Exposed functions can be called from the view.
    vm.goEdit = goEdit;

    // Initializations --------------------------------------------------------------------------------------------
    // Functions that are run when view/html is loaded
    // init is a private function (i.e., not exposed)
    init();

    // Function declaration and definition -------------------------------------------------------------------------
    function goEdit(empNo){
        $state.go("editWithParam",{empNo : empNo});
    }

    // The init function initializes view
    function init() {
      MyAppService.getUsersList('')
        .then(function (results) {
          console.log(results);
          vm.users = results;
        })
        .catch(function (err) {
          console.log("error " + err);
        });
    }

  }
  
})();