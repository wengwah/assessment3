(function () {
  "use strict";
  angular
    .module("MyApp")
    .service("AuthService", AuthService);

    AuthService.$inject = ["$http", "$q"];

    function AuthService($http, $q) {
        var self = this;
        var user = null;
        self.register = function(){

        }

        self.login = function(userProfile){
            console.log(userProfile);
            var deferred = $q.defer();
            $http.post("/security/login", userProfile)
                .then(function(data){
                    console.log(data);
                    if(data.status == 200){
                        deferred.resolve();
                    }
                }).catch(function (error){
                    console.log(error);
                    user = false;
                    deferred.reject();
                })
            return deferred.promise;
        }

        self.isUserLoggedIn = function(cb){
            $http.get("/status/user").then(function(data){
                console.log(data);
                user = true;
                cb(user);
            }).catch(function(error){
                console.log(error);
                user= false;
                cb(user);
            })
        }

        self.logout = function(){
            var deferred = $q.defer();
            $http.get("/logout")
                .then(function(data){
                    console.log(data);
                    if(data.status == 200){
                        deferred.resolve();
                    }
                }).catch(function (error){
                    console.log(error);
                    user = false;
                    deferred.reject();
                })
            return deferred.promise;
        }
    }
})();