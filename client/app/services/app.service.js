(function () {
  "use strict";
  angular
    .module("MyApp")
    .service("MyAppService", MyAppService);

  MyAppService.$inject = ['$http', '$q'];

  function MyAppService($http, $q) {
      
    var service = this;
    
    // EXPOSED DATA MODELS -----------------------------------------------------------------------------------------

    // EXPOSED FUNCTIONS -------------------------------------------------------------------------------------------
    service.getUser = getUser;
    service.getUsersList = getUsersList;
    service.createUser = createUser;
    service.updateUser = updateUser;
    service.deleteUser = deleteUser;

    // Basic CRUD operations
    function getUser(user_id) {
      var defer = $q.defer();

      $http.get("api/users/" + user_id)
        .then(function (result) {
          defer.resolve(result.data);
        })
        .catch(function (error) {
          defer.reject(error);
        })
        return (defer.promise);
    }

    function getUsersList() {
      var defer = $q.defer();

      $http.get("api/users")
        .then(function (result) {
          defer.resolve(result.data);
        })
        .catch(function (error) {
          defer.reject(error);
        })
        return (defer.promise);
    }    

    function createUser(user) {
      var defer = $q.defer();

      $http.post("api/users")
        .then(function (result) {
          defer.resolve(result.data);
        })
        .catch(function (error) {
          defer.reject(error);
        })
        return (defer.promise);
    }

    function updateUser(user_id, user_name) {
      var defer = $q.defer();

      $http.put("api/users/" + user_id)
        .then(function (result) {
          defer.resolve(result.data);
        })
        .catch(function (error) {
          defer.reject(error);
        })
        return (defer.promise);
    }

    function deleteUser(user_id) {
      var defer = $q.defer();

      $http.delete("api/users/" + user_id)
        .then(function (result) {
          defer.resolve(result.data);
        })
        .catch(function (error) {
          defer.reject(error);
        })
        return (defer.promise);      
    }

    // Search operations
    // function retrieveEmpDB(searchString){
    //   return $http({
    //     method: 'GET',
    //     url: 'api/employees',
    //     params: {
    //       'searchString': searchString
    //     }
    //   });
    // }

    // function retrieveEmpDept(searchString){
    //   return $http({
    //     method: 'GET',
    //     url: 'api/employees/departments',
    //     params: {
    //       'searchString': searchString
    //     }
    //   });
    // }    

  } // end MyAppService function

}) ();