(function() {
  "use strict";
  angular
    .module("MyApp", [
      "ui.router", 
      "ngSanitize", 
      "ngMessages", 
      "ngAnimate"
    ]);
        
})();